Moonlight Knights hail from the Gates of Morning, the last bastion of
the free people in the Far East.

Their way of life is well represented by their motto 'The Moon is our
protector in the darkness, and so shall we be for all free peoples.'

Though secretive, they channel the power of the Moon to protect all
who call the Sunwall home.

Specialized in using the weapons and magic of their enemies against
them, they usually protect themselves with the power of the Moon,
while destroying their enemies in melee.

Their most important stats are: Strength and Magic

#### Stat modifiers:
* +4 Strength, -1 Dexterity, +2 Constitution
* +4 Magic, +0 Willpower, +0 Cunning

**Life per level:** *2*
