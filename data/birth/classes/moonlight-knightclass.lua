-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2017 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Thea Barnes
-- thea.skye.barnes@gmail.com

local Particles = require "engine.Particles"
getBirthDescriptor("class", "Celestial").descriptor_choices.subclass["Moonlight Knight"] = "allow"

newBirthDescriptor{
	type = "subclass",
	name = "Moonlight Knight",
	desc = {
		"Moonlight Knights hail from the Gates of Morning, the last bastion of the free people in the Far East.",
		"Their way of life is well represented by their motto 'The Moon is our protector in the darkness, and so shall we be for all free peoples.'",
		"Though secretive, they channel the power of the Moon to protect all who call the Sunwall home.",
		"Competent with exotic weapons to fight the Naga, they usually protect themselves with the power of the Moon, while bashing their enemies in melee.",
		"Their most important stats are: Strength, Dexterity, and Magic",
		"#GOLD#Stat modifiers:",
		"#LIGHT_BLUE# * +3 Strength, +3 Dexterity, +0 Constitution",
		"#LIGHT_BLUE# * +3 Magic, +0 Willpower, +0 Cunning",
		"#GOLD#Life per level:#LIGHT_BLUE# 2"
	},
	power_source = {technique=true, arcane=true},
	stats = { str=3, dex=3, mag=3 },
	talents_types = {
		-- Class Skills
		["celestial/night"]={true, 0.3},
		["celestial/star-fury"]={true, 0.3},
		["technique/2hweapon-assault"]={true, 0.1},
		["technique/duelist"]={true, 0.1},
		["technique/combat-techniques-active"]={false, 0.1},
		["technique/combat-techniques-passive"]={true, 0.1},
		["celestial/moonlight-combat"]={true, 0.3},

		-- Generic Skills
		["technique/combat-training"]={true, 0.3},
		["cunning/survival"]={false, 0.1},
		["celestial/hymns"]={true, 0.3},
		["celestial/moon"]={true, 0.3},

		-- Level 10+ Skills
		-- TODO: Add new talents for lvl 10+
	},
	talents = {
		[ActorTalents.T_LUNAR_WEAPONS] = 1,
		[ActorTalents.T_EXOTIC_WEAPONS_MASTERY] = 1,
		[ActorTalents.T_EMBRACE_OF_LUNA] = 1,
		[ActorTalents.T_TRIDENT_MASTER] = 1,
		[ActorTalents.T_ARMOUR_TRAINING] = 1,
	},
	copy = {
		max_life = 110,
		resolvers.equipbirth{ id=true,
			{type="weapon", subtype="trident", name="coral trident", autoreq=true, ego_chance=-1000},
			{type="armor", subtype="heavy", name="iron mail armour", autoreq=true, ego_chance=-1000},
		},
		resolvers.inventorybirth{ id=true, inven="QS_MAINHAND",
			{type="weapon", subtype="whip", name="rough leather whip", autoreq=true, ego_chance=-1000},
		},
		resolvers.inventorybirth{ id=true, inven="QS_OFFHAND",
			{type="weapon", subtype="dagger", name="iron dagger", autoreq=true, ego_chance=-1000},	
		},
	},
	copy_add = {
		life_rating = 2,
	},
}
