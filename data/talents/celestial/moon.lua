-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2017 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Thea Barnes
-- thea.skye.barnes@gmail.com

newTalent{
	name = "Embrace of Luna",
	type = {"celestial/moon", 1},
	mode = "sustained",
	points = 5,
	cooldown = 5,
	sustain_negative = 5,
	require = mags_req1,
	range = 10,
	tactical = { BUFF = 2 },
	getRegen = function(self, t) return self:combatTalentScale(t, 1/7, 5/7, 0.75) end,
	getCritChance = function(self, t) return self:combatTalentScale(t, 2.5, 11, 0.75) end,
	activate = function(self, t)
		local power = t.getRegen(self, t)
		local crit = t.getCritChance(self, t)
		return {
			regen = self:addTemporaryValue("negative_regen", power),
			pc = self:addTemporaryValue("combat_physcrit", crit),
			sc = self:addTemporaryValue("combat_spellcrit", crit),
		}
	end,
	deactivate = function(self, t, p)
		self:removeTemporaryValue("negative_regen", p.regen)
		self:removeTemporaryValue("combat_physcrit", p.pc)
		self:removeTemporaryValue("combat_spellcrit", p.sc)
		return true
	end,
	info = function(self, t)
		return ([[Regenerates %0.2f negative energy per turn, and increases physical and spell critical chance by %d%% while active.]]):format(t.getRegen(self, t), t.getCritChance(self, t))
	end,
}

newTalent{
	name = "Luna's Grace",
	type = {"celestial/moon", 2},
	require = mags_req2,
	points = 5,
	random_ego = "defensive",
	cooldown = 10,
	negative = -10,
	tactical = { HEAL = 2 },
	getHeal = function(self, t) return self:combatTalentSpellDamage(t, 20, 440) end,
	is_heal = true,
	action = function(self, t)
		self:attr("allow_on_heal", 1)
		self:heal(self:spellCrit(t.getHeal(self, t)), self)
		self:attr("allow_on_heal", -1)
		if core.shader.active(4) then
			self:addParticles(Particles.new("shader_shield_temp", 1, {toback=true, size_factor=1.5, y=-0.3, img="healcelestial", life=25}, {type="healing", time_factor=2000, beamsCount=20, noup=2.0, beamColor1={0xd8/255, 0xff/255, 0x21/255, 1}, beamColor2={0xf7/255, 0xff/255, 0x9e/255, 1}, circleDescendSpeed=3}))
			self:addParticles(Particles.new("shader_shield_temp", 1, {toback=false,size_factor=1.5, y=-0.3, img="healcelestial", life=25}, {type="healing", time_factor=2000, beamsCount=20, noup=1.0, beamColor1={0xd8/255, 0xff/255, 0x21/255, 1}, beamColor2={0xf7/255, 0xff/255, 0x9e/255, 1}, circleDescendSpeed=3}))
		end
		game:playSoundNear(self, "talents/heal")
		return true
	end,
	info = function(self, t)
		local heal = t.getHeal(self, t)
		return ([[Call upon the power of the Moon to heal your body for %d life.
		The amount healed will increase with your Spellpower.]]):
		format(heal)
	end,
}

newTalent{
	name = "Lunar Shield",
	type = {"celestial/moon", 3},
	require = mags_req3,
	points = 5,
	random_ego = "defensive",
	negative = -20,
	cooldown = 15,
	tactical = { DEFEND = 2 },
	getAbsorb = function(self, t) return self:combatTalentSpellDamage(t, 30, 370) end,
	action = function(self, t)
		self:setEffect(self.EFF_DAMAGE_SHIELD, 10, {color={0xe1/255, 0xcb/255, 0x3f/255}, power=self:spellCrit(t.getAbsorb(self, t))})
		game:playSoundNear(self, "talents/heal")
		return true
	end,
	info = function(self, t)
		local absorb = t.getAbsorb(self, t)
		return ([[A protective shield forms around you that lasts for up to 10 turns and negates %d damage.
		The total damage the shield can absorb will increase with your Spellpower.]]):
		format(absorb)
	end,
}

newTalent{
	name = "Blue Moon",
	type = {"celestial/moon", 4},
	mode = "passive",
	require = mags_req4,
	points = 5,
	getCrit = function(self, t) return self:combatTalentScale(t, 3, 15, 0.75) end,
	passives = function(self, t, p)
		self:talentTemporaryValue(p, "combat_physcrit", t.getCrit(self, t))
		self:talentTemporaryValue(p, "combat_spellcrit", t.getCrit(self, t))
	end,
	info = function(self, t)
		return ([[Increases your physical and spell critical chances by %d%%.]]):
		format(t.getCrit(self, t))
	end,
}
