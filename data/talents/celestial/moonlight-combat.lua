-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2018 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Thea Barnes
-- thea.skye.barnes@gmail.com

-- This concept plays well but vs. low damage levels spam bumping can make stupidly large shields
-- Leaving as is for now but will likely change somehow

newTalent{
	name = "Trident Master",
	type = {"celestial/moonlight-combat", 1},
	require = {
		stat = { str=10 },
		level = 0,
	},
	innate = true,
	hide = true,
	mode = "passive",
	points = 1,
	no_unlearn_last = true,
	on_learn = function(self, t)
		table.set(self, "__show_special_talents", Talents.T_EXOTIC_WEAPONS_MASTERY, true)
	end,
	info = function(self, t)
		return ([[Reveals the Exotic Weapons Mastery talent, which allows training with tridents.]]):
		format()
	end,
}

newTalent{
	name = "Lunar Weapons",
	type = {"celestial/moonlight-combat", 1},
	mode = "sustained",
	require = mags_req1,
	points = 5,
	cooldown = 10,
	sustain_negative = 10,
	tactical = { BUFF = 2 },
	range = 10,
	getDamage = function(self, t) return 7 + self:combatSpellpower(0.092) * self:combatTalentScale(t, 1, 7) end,
	getShieldFlat = function(self, t)
		return t.getDamage(self, t) / 2
	end,
	activate = function(self, t)
		game:playSoundNear(self, "talents/spell_generic2")
		local ret = {
			dam = self:addTemporaryValue("melee_project", {[DamageType.DARKNESS]=t.getDamage(self, t)}),
		}
		return ret
	end,
	deactivate = function(self, t, p)
		self:removeTemporaryValue("melee_project", p.dam)
		return true
	end,
	callbackOnMeleeAttack = function(self, t, target, hitted, crit, weapon, damtype, mult, dam)
		if hitted and self:hasEffect(self.EFF_DAMAGE_SHIELD) and (self:reactionToward(target) < 0) then
			-- Shields can't usually merge, so change the parameters manually
			local shield = self:hasEffect(self.EFF_DAMAGE_SHIELD)
			local shield_power = t.getShieldFlat(self, t)

			shield.power = shield.power + shield_power
			self.damage_shield_absorb = self.damage_shield_absorb + shield_power
			self.damage_shield_absorb_max = self.damage_shield_absorb_max + shield_power
			shield.dur = math.max(2, shield.dur)

			-- Limit the number of times a shield can be extended, Bathe in Light also uses this code
			if shield.dur_extended then
				shield.dur_extended = shield.dur_extended + 1
				if shield.dur_extended >= 20 then
					game.logPlayer(self, "#DARK_ORCHID#Your damage shield cannot be extended any farther and has exploded.")
					self:removeEffect(self.EFF_DAMAGE_SHIELD)
				end
			else shield.dur_extended = 1 end
		end

	end,
	info = function(self, t)
		local damage = t.getDamage(self, t)
		local shieldflat = t.getShieldFlat(self, t)
		return ([[Infuse your weapon with the power of the Moon, adding %0.1f darkness damage on each melee hit.
		Additionally, if you have a temporary damage shield active, melee attacks will increase its power by %d.
		If the same shield is refreshed 20 times it will become unstable and explode, removing it.
		The damage dealt and shield bonus will increase with your Spellpower.]]):
		format(damDesc(self, DamageType.DARKNESS, damage), shieldflat)
	end,
}

-- A potentially very powerful ranged attack that gets more effective with range
-- 2nd attack does reduced damage to balance high damage on 1st attack (so that the talent is always useful at low levels and close ranges)
newTalent{
	name = "Moonlight Strike",
	type = {"celestial/moonlight-combat",2},
	require = mags_req2,
	points = 5,
	random_ego = "attack",
	cooldown = 6,
	negative = 15,
	tactical = { ATTACK = 2 },
	requires_target = true,
	is_melee = true,
	range = function(self, t) return 2 + math.max(0, self:combatStatScale("str", 0.8, 8)) end,
	SecondStrikeChance = function(self, t, range)
		return self:combatLimit(self:getTalentLevel(t)*range, 100, 15, 4, 70, 50)
	end, -- 15% for TL 1.0 at range 4, 70% for TL 5.0 at range 10
	getDamage = function(self, t, second)
		if second then
			return self:combatTalentWeaponDamage(t, 0.9, 2)*self:combatTalentLimit(t, 1.0, 0.4, 0.65)
		else
			return self:combatTalentWeaponDamage(t, 0.9, 2)
		end
	end,
	action = function(self, t)
		local tg = {type="hit", range=self:getTalentRange(t), talent=t}
		local x, y = self:getTarget(tg)
		if not x or not y then return nil end
		local _ _, x, y = self:canProject(tg, x, y)
		local target = game.level.map(x, y, Map.ACTOR)
		if target then
			self:attackTarget(target, nil, t.getDamage(self, t), true)
			local range = core.fov.distance(self.x, self.y, target.x, target.y)
			if range > 1 and rng.percent(t.SecondStrikeChance(self, t, range)) then
				game.logSeen(self, "#CRIMSON#"..self.name.."strikes twice with Wave of Power!#NORMAL#")
				self:attackTarget(target, nil, t.getDamage(self, t, true), true)
			end
		else
			return
		end
		return true
	end,
	info = function(self, t)
		local range = self:getTalentRange(t)
		return ([[In a pure display of power, you project a ranged melee attack, doing %d%% weapon damage.
		If the target is outside of melee range, you have a chance to project a second attack against it for %d%% weapon damage.
		The second strike chance (which increases with distance) is %0.1f%% at range 2 and %0.1f%% at the maximum range of %d.
		The range will increase with your Strength.]]):
		format(t.getDamage(self, t)*100, t.getDamage(self, t, true)*100, t.SecondStrikeChance(self, t, 2), t.SecondStrikeChance(self, t, range), range)
	end,
}

-- Interesting interactions with shield timing, lots of synergy and antisynergy in general
newTalent{
	name = "Luna's Wrath",
	type = {"celestial/moonlight-combat", 3},
	mode = "sustained",
	require = mags_req3,
	points = 5,
	cooldown = 10,
	sustain_negative = 10,
	tactical = { BUFF = 2 },
	range = 10,
	getMartyrDamage = function(self, t) return self:combatTalentLimit(t, 50, 5, 25) end, --Limit < 50%
	getLifeDamage = function(self, t) return self:combatTalentLimit(t, 1.0, 0.1, 0.8) end, -- Limit < 100%
	getMaxDamage = function(self, t) return self:combatTalentSpellDamage(t, 10, 400) end,
	getDamage = function(self, t)
		local damage = (self:attr("weapon_of_wrath_life") or t.getLifeDamage(self, t)) * (self.max_life - math.max(0, self.life)) -- avoid problems with die_at
		return math.min(t.getMaxDamage(self, t), damage) -- The Martyr effect provides the upside for high HP NPC's
	end,
	activate = function(self, t)
		game:playSoundNear(self, "talents/spell_generic2")
		-- Is this any better than having the callback call getLifeDamage?  I figure its better to calculate it once
		local ret = {
			martyr = self:addTemporaryValue("weapon_of_wrath_martyr", t.getMartyrDamage(self, t)),
			damage = self:addTemporaryValue("weapon_of_wrath_life", t.getLifeDamage(self, t)),
		}
		return ret
	end,
	deactivate = function(self, t, p)
		self:removeTemporaryValue("weapon_of_wrath_martyr", p.martyr)
		self:removeTemporaryValue("weapon_of_wrath_life", p.damage)
		return true
	end,
	callbackOnMeleeAttack = function(self, t, target, hitted, crit, weapon, damtype, mult, dam)
		if hitted and self:attr("weapon_of_wrath_martyr") and not self.turn_procs.weapon_of_wrath and not target.dead then
			target:setEffect(target.EFF_MARTYRDOM, 4, {power = self:attr("weapon_of_wrath_martyr")})
			local damage = t.getDamage(self, t)
			if damage == 0 then return end
			local tg = {type="hit", range=10, selffire=true, talent=t}
			self:project(tg, target.x, target.y, DamageType.DARKNESS, damage)
			self.turn_procs.weapon_of_wrath = true
		end
	end,
	info = function(self, t)
		local martyr = t.getMartyrDamage(self, t)
		local damagepct = t.getLifeDamage(self, t)
		local damage = t.getDamage(self, t)
		return ([[Your weapon attacks burn with righteous fury, dealing %d%% of your lost HP as additional Darkness damage (up to %d, Current:  %d).
		Targets struck are also afflicted with a Martyrdom effect that causes them to take %d%% of all damage they deal for 4 turns.]]):
		format(damagepct*100, t.getMaxDamage(self, t, 10, 400), damage, martyr)
	end,
}

newTalent{
	name = "Moonlight Thorns",
	type = {"celestial/moonlight-combat", 4},
	mode = "sustained",
	require = mags_req4,
	points = 5,
	sustain_mana = 20,
	cooldown = 10,
	tactical = { BUFF = 2 },
	getDamage = function(self, t) return self:combatTalentSpellDamage(t, 20, 120) end,
	activate = function(self, t)
		game:playSoundNear(self, "talents/heal")
		return {
			particle = self:addParticles(Particles.new("phantasm_shield", 1)),
			onhit = self:addTemporaryValue("on_melee_hit", {[DamageType.DARKNESS]=t.getDamage(self, t)}),
		}
	end,
	deactivate = function(self, t, p)
		self:removeParticles(p.particle)
		self:removeTemporaryValue("on_melee_hit", p.onhit)
		return true
	end,
	info = function(self, t)
		local damage = t.getDamage(self, t)
		return ([[The caster is surrounded by a shield of moonlight. If hit in melee, the shield will deal %d darkness damage to the attacker.
		The damage will increase with your Spellpower.]]):
		format(damDesc(self, DamageType.LIGHT, damage))
	end,
}
