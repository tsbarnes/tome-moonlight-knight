-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2017 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Thea Barnes
-- thea.skye.barnes@gmail.com

newTalentType{ allow_random=true, type="celestial/moon", name = "moon", generic = true, is_spell = true, description = "Gain the protection and favor of Luna." }
newTalentType{ allow_random=true, type="celestial/moonlight-combat", name = "moonlight combat", is_spell = true, description = "Harness the power of the Moon to boost your combat abilities." }
newTalentType{ allow_random=true, type="celestial/night", name = "night", is_spell = true, description = "Drive your enemies to despair with haunting nocturnes." }
newTalentType{ allow_random=true, type="celestial/shadows", name = "shadows", is_spell = true, description = "Your connection to the Moon makes you at home in the shadows." }

-- Generic requires for magic based on talent level
mags_req1 = {
	stat = { mag=function(level) return 12 + (level-1) * 2 end },
	level = function(level) return 0 + (level-1)  end,
}
mags_req2 = {
	stat = { mag=function(level) return 20 + (level-1) * 2 end },
	level = function(level) return 4 + (level-1)  end,
}
mags_req3 = {
	stat = { mag=function(level) return 28 + (level-1) * 2 end },
	level = function(level) return 8 + (level-1)  end,
}
mags_req4 = {
	stat = { mag=function(level) return 36 + (level-1) * 2 end },
	level = function(level) return 12 + (level-1)  end,
}
mags_req5 = {
	stat = { mag=function(level) return 44 + (level-1) * 2 end },
	level = function(level) return 16 + (level-1)  end,
}
mags_req_high1 = {
	stat = { mag=function(level) return 22 + (level-1) * 2 end },
	level = function(level) return 10 + (level-1)  end,
}
mags_req_high2 = {
	stat = { mag=function(level) return 30 + (level-1) * 2 end },
	level = function(level) return 14 + (level-1)  end,
}
mags_req_high3 = {
	stat = { mag=function(level) return 38 + (level-1) * 2 end },
	level = function(level) return 18 + (level-1)  end,
}
mags_req_high4 = {
	stat = { mag=function(level) return 46 + (level-1) * 2 end },
	level = function(level) return 22 + (level-1)  end,
}
mags_req_high5 = {
	stat = { mag=function(level) return 54 + (level-1) * 2 end },
	level = function(level) return 26 + (level-1)  end,
}



damDesc = function(self, type, dam)
	if self:attr("dazed") then
		dam = dam * 0.5
	end
	if self:attr("stunned") then
		dam = dam * 0.4
	end
	if self:attr("invisible_damage_penalty") then
		dam = dam * util.bound(1 - (self.invisible_damage_penalty / (self.invisible_damage_penalty_divisor or 1)), 0, 1)
	end
	if self:attr("numbed") then
		dam = dam - dam * self:attr("numbed") / 100
	end
	if self:attr("generic_damage_penalty") then
		dam = dam - dam * math.min(100, self:attr("generic_damage_penalty")) / 100
	end

	-- Increases damage
	if self.inc_damage then
		local inc = self:combatGetDamageIncrease(type)
		dam = dam + (dam * inc / 100)
	end
	return dam
end


-----------------------------

load("/data-moonlight-knight/talents/celestial/moon.lua")
load("/data-moonlight-knight/talents/celestial/moonlight-combat.lua")
load("/data-moonlight-knight/talents/celestial/night.lua")
