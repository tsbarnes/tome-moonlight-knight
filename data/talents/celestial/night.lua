-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2017 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Thea Barnes
-- thea.skye.barnes@gmail.com

newTalent{
  name = "Spear Nocturne",
  type = {"celestial/night", 1},
  require = mags_req1,
  random_ego = "attack",
  points = 5,
  cooldown = 6,
  negative = -16,
  range = 7,
  tactical = { ATTACK = {DARKNESS = 2} },
  direct_hit = true,
  reflectable = true,
  requires_target = true,
  getDamage = function(self, t) return self:combatTalentSpellDamage(t, 6, 160) end,
	getDamageOnSpot = function(self, t) return self:combatTalentSpellDamage(t, 6, 80) end,
  action = function(self, t)
    local tg = {type="hit", range=self:getTalentRange(t), talent=t}
    local x, y = self:getTarget(tg)
    if not x or not y then return nil end
    self:project(tg, x, y, DamageType.DARKNESS, self:spellCrit(t.getDamage(self, t)), {type="darkness"})

    local _ _, x, y = self:canProject(tg, x, y)
    -- Add a lasting map effect
    game.level.map:addEffect(self,
      x, y, 4,
      DamageType.DARKNESS, t.getDamageOnSpot(self, t),
      0,
      5, nil,
      {type="light_zone"},
      nil, self:spellFriendlyFire()
    )

    game:playSoundNear(self, "talents/flame")
    return true
  end,
  info = function(self, t)
    local damage = t.getDamage(self, t)
    local damageonspot = t.getDamageOnSpot(self, t)
    return ([[Calls the power of the Moon into a spear of darkness, doing %0.2f damage to the target and leaving a spot on the ground for 4 turns that does %0.2f darkness damage to anyone within it.
    The damage dealt will increase with your Spellpower.]]):
    format(damDesc(self, DamageType.DARKNESS, damage), damageonspot)
  end,
}

newTalent{
	name = "Sphere Nocturne",
	type = {"celestial/night", 2},
	require = mags_req2,
	random_ego = "utility",
	points = 5,
	negative = -15,
	cooldown = 14,
	range = 0,
  radius = function(self, t) return math.floor(self:combatTalentScale(t, 6, 10)) end,
  tactical = {
    ATTACKAREA = function(self, t)
      if self:getTalentLevel(t) >= 3 then
        return { LIGHT = 2 }
      end
      return 0
    end,
	},
	getDamage = function(self, t) return self:combatTalentSpellDamage(t, 28, 180) end,
	getBlindPower = function(self, t) if self:getTalentLevel(t) >= 5 then return 4 else return 3 end end,
	requires_target = true,
	target = function(self, t) return {type="ball", range=self:getTalentRange(t), selffire=false, radius=self:getTalentRadius(t), talent=t} end,
	action = function(self, t)
		local tg = self:getTalentTarget(t)
		game.level.map:particleEmitter(self.x, self.y, tg.radius, "sunburst", {radius=tg.radius, grids=grids, tx=self.x, ty=self.y, max_alpha=80})
		if self:getTalentLevel(t) >= 3 then
			self:project(tg, self.x, self.y, DamageType.LIGHT, self:spellCrit(t.getDamage(self, t)))
		end
		tg.selffire = true
		self:project(tg, self.x, self.y, DamageType.LITE, 1)
		game:playSoundNear(self, "talents/heal")
		return true
	end,
	info = function(self, t)
		local radius = self:getTalentRadius(t)
		local turn = t.getBlindPower(self, t)
		local dam = t.getDamage(self, t)
		return ([[Creates a globe of moonlight within a radius of %d that illuminates the area.
		At level 3, it also deals %0.2f light damage.]]):
		format(radius, turn, damDesc(self, DamageType.LIGHT, dam))
	end,
}

newTalent{
	name = "Fear Nocturne",
	type = {"celestial/night", 3},
	require = mags_req3,
	points = 5,
	random_ego = "attack",
	negative = -20,
	cooldown = 10,
	direct_hit = true,
	tactical = { ATTACKAREA = { DARKNESS = 2 }, DISABLE = { knockback = 2 }, ESCAPE = { knockback = 1 } },
	range = 0,
	radius = function(self, t) return math.floor(self:combatTalentScale(t, 4, 8)) end,
	requires_target = true,
	target = function(self, t) return {type="cone", range=self:getTalentRange(t), radius=self:getTalentRadius(t), friendlyfire=isFF(self), talent=t} end,
	getDamage = function(self, t) return self:combatTalentSpellDamage(t, 10, 230) end,
	action = function(self, t)
		local tg = self:getTalentTarget(t)
		local x, y = self:getTarget(tg)
		if not x or not y then return nil end
		self:project(tg, x, y, DamageType.DARKKNOCKBACK, {dist=4, dam=self:spellCrit(t.getDamage(self, t))})
		game.level.map:particleEmitter(self.x, self.y, tg.radius, "breath_dark", {radius=tg.radius, tx=x-self.x, ty=y-self.y})
		game:playSoundNear(self, "talents/fire")
		return true
	end,
	info = function(self, t)
		local damage = t.getDamage(self, t)
		local radius = self:getTalentRadius(t)
		return ([[Invoke a cone of nightfall dealing %0.2f darkness damage in a radius of %d. Any creatures caught inside must make check against their Mental Save or be knocked back 4 grids away.
		The damage will increase with your Spellpower.]]):
		format(damDesc(self, DamageType.DARKNESS, damage), self:getTalentRadius(t))
	end,
}

newTalent{
	name = "Nyctophobia",
	type = {"celestial/night", 4},
	points = 5,
	require = mags_req4,
	cooldown = 8,
	negative = -10,
	tactical = { DISABLE = {sleep = 1}, ATTACK = { DARKNESS = 2 }, },
	direct_hit = true,
	requires_target = true,
	range = 7,
	target = function(self, t) return {type="cone", radius=self:getTalentRange(t), range=0, talent=t, selffire=false} end,
	getDuration = function(self, t) return math.floor(self:combatTalentScale(t, 2.5, 4.5)) end,
	getSleepPower = function(self, t) 
		local power = self:combatTalentSpellDamage(t, 5, 25)
		return math.ceil(power)
	end,
	getDamage = function(self, t) return self:combatTalentSpellDamage(t, 10, 50) end,
	action = function(self, t)
		local tg = self:getTalentTarget(t)
		local x, y = self:getTarget(tg)
		if not x or not y then return nil end
		
		local damage = self:spellCrit(t.getDamage(self, t))
		local power = self:spellCrit(t.getSleepPower(self, t))
		self:project(tg, x, y, function(tx, ty)
			local target = game.level.map(tx, ty, Map.ACTOR)
			if target then
				if target:canBe("sleep") then
					target:setEffect(target.EFF_NIGHTMARE, t.getDuration(self, t), {src=self, power=power, waking=is_waking, dam=damage, insomnia=20, no_ct_effect=true, apply_power=self:combatSpellpower()})
				else
					game.logSeen(self, "%s resists the nightmare!", target.name:capitalize())
				end
			end
		end)
		
		game.level.map:particleEmitter(self.x, self.y, tg.radius, "generic_wave", {radius=tg.radius, tx=x-self.x, ty=y-self.y, rm=60, rM=130, gm=20, gM=110, bm=90, bM=130, am=35, aM=90})
		game:playSoundNear(self, "talents/breath")
		return true
	end,
	info = function(self, t)
		local radius = self:getTalentRange(t)
		local duration = t.getDuration(self, t)
		local power = t.getSleepPower(self, t)
		local damage = t.getDamage(self, t)
		return([[Puts targets in a radius %d cone into a restless sleep for %d turns, rendering them unable to act.  Every %d points of damage the target suffers will reduce the effect duration by one turn.
		Each turn, they'll suffer %0.2f darkness damage.  This damage will not reduce the duration of the effect.
		When the targets wake, they will suffer from Insomnia for a number of turns equal to the amount of time it was asleep (up to ten turns max), granting it 20%% sleep immunity for each turn of the Insomnia effect.
		The damage threshold and mind damage will scale with your Spellpower.]]):format(radius, duration, power, damDesc(self, DamageType.DARKNESS, (damage)))
	end,
}
