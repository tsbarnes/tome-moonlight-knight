-- ToME - Tales of Maj'Eyal
-- Copyright (C) 2009 - 2017 Nicolas Casalini
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- Thea Barnes
-- thea.skye.barnes@gmail.com

newTalent{
  name = "Shadow Enhancement",
  type = {"celestial/shadows", 1},
  require = mags_req1,
  points = 5,
	mode = "sustained",
	getStealthPower = function(self, t) return self:combatTalentScale(t, 7.5, 30, 0.1) end,
	on_pre_use = function(self, t, silent, fake)
		local armor = self:getInven("BODY") and self:getInven("BODY")[1]
		if armor and (armor.subtype == "massive") then
			if not silent then game.logPlayer(self, "You cannot be stealthy with such heavy armour on!") end
			return nil
		end
		if self:isTalentActive(t.id) then return true end
		
		-- Check nearby actors detection ability
		if not self.x or not self.y or not game.level then return end
		if not rng.percent(self.hide_chance or 0) then
			if stealthDetection(self, t.getRadius(self, t)) > 0 then
				if not silent then game.logPlayer(self, "You are being observed too closely to enter Stealth!") end
				return nil
			end
		end
		return true
	end,
	sustain_lists = "break_with_stealth",
	passives = function(self, t, p)
		self:talentTemporaryValue(p, "inc_stats", {
			[self.STAT_DEX] = math.floor(2 * self:getTalentLevel(t)),
			[self.STAT_CUN] = math.floor(2 * self:getTalentLevel(t)),
		})
	end,
	activate = function(self, t)
		game:playSoundNear(self, "talents/spell_generic2")
		local ret = {
			sb = self:addTemporaryValue("stealth", t.getStealthPower(self, t)),
		}
		return ret
	end,
	deactivate = function(self, t, p)
		self:removeTemporaryValue("stealth", p.sb)
		return true
	end,
	info = function(self, t)
		local statboost = math.floor(2 * self:getTalentLevel(t))
		local power = t.getStealthPower(self, t)
		return ([[Your connection to the Moon makes you at home in the shadows.
Increases stealth by %d while active. Actions that break stealth will deactivate this talent.
Additionally, passively increases Dexterity and Cunning by %d.]]):
		format(power, statboost)
	end,
}

newTalent{
	name = "Shadow Lethality",
	type = {"celestial/shadows", 2},
	mode = "passive",
	points = 5,
	require = mags_req2,
	critpower = function(self, t) return self:combatTalentScale(t, 7.5, 30, 0.1) end,
	getCriticalChance = function(self, t) return self:combatTalentScale(t, 2.5, 10, 0.1) end,
	passives = function(self, t, p)
		self:talentTemporaryValue(p, "combat_generic_crit", t.getCriticalChance(self, t))
		self:talentTemporaryValue(p, "combat_critical_power", t.critpower(self, t))
	end,
	info = function(self, t)
		local critchance = t.getCriticalChance(self, t)
		local power = t.critpower(self, t)
		return ([[You use your connection to the darkness to hit your opponent's weak spots.
All your strikes have a %0.1f%% greater chance to be critical hits, and your critical hits do %0.1f%% more damage.]]):
		format(critchance, power)
	end,
}

newTalent{
	name = "Moon Renewal",
	type = {"celestial/shadows", 3},
	require = mags_req3,
	points = 5,
	negative = 25,
	cooldown = 30,
	tactical = { BUFF = 1 },
	fixed_cooldown = true,
	getTalentCount = function(self, t) return math.floor(self:combatTalentScale(t, 2, 7, "log")) end,
	getMaxLevel = function(self, t) return math.max(1, self:getTalentLevelRaw(t)) end,
	speed = "combat",
	action = function(self, t)
		local tids = {}
		for tid, _ in pairs(self.talents_cd) do
			local tt = self:getTalentFromId(tid)
			if not tt.fixed_cooldown then
				if tt.type[2] <= t.getMaxLevel(self, t) and (tt.type[1]:find("^cunning/") or tt.type[1]:find("^technique/")) then
					tids[#tids+1] = tid
				end
			end
		end
		for i = 1, t.getTalentCount(self, t) do
			if #tids == 0 then break end
			local tid = rng.tableRemove(tids)
			self.talents_cd[tid] = nil
		end
		self.changed = true
		return true
	end,
	info = function(self, t)
		local talentcount = t.getTalentCount(self, t)
		local maxlevel = t.getMaxLevel(self, t)
		return ([[The revitalizing power of the shadows allows you to reset the cooldown of up to %d of your combat talents (cunning or technique) of tier %d or less.]]):
		format(talentcount, maxlevel)
	end,
}
