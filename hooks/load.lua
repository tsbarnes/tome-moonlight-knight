local DamageType = require "engine.DamageType"
local class = require "engine.class"
local Birther = require "engine.Birther"
local ActorTalents = require "engine.interface.ActorTalents"
local ActorTemporaryEffects = require "engine.interface.ActorTemporaryEffects"

class:bindHook("ToME:load", function(self, data)
	ActorTalents:loadDefinition("/data-moonlight-knight/talents/celestial/moonlight-knight.lua")
	Birther:loadDefinition("/data-moonlight-knight/birth/classes/moonlight-knightclass.lua")
end)
