-- Moonlight Knight
-- moonlight-knight/init.lua

long_name = "Moonlight Knight"
short_name = "moonlight-knight" -- Determines the name of your addon's file.
for_module = "tome"
version = {1,5,10}
addon_version = {1,0,0}
weight = 100 -- The lower this value, the sooner your addon will load compared to other addons.
author = {'thea.skye.barnes@gmail.com'}
homepage = ''
description = [[Adds the Moonlight Knight class
]] -- the [[ ]] things are like quote marks that can span multiple lines
tags = {'celestial', 'paladin', 'moon', 'moonlight', 'knight'} -- tags MUST immediately follow description

overload = true
superload = true
data = true
hooks = true